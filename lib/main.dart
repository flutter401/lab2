import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());
class HelloFlutterApp extends StatefulWidget{
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String english ="Hello Flutter";
String spainish = "Hola Flutter";
String japanish = "ありがとうございました";
String korea = "AnyongAseyo";
class _HelloFlutterAppState extends State<HelloFlutterApp>{
  String displayText= english;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello IAMINW"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText ==english? english:english;
                  });
                },
                icon: Icon(Icons.language))
            ,
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText ==english? japanish:japanish;
                  });
                },
                icon: Icon(Icons.translate)),
            IconButton(
                onPressed: (){
                  setState(() {
                    displayText = displayText ==english? korea:korea;
                  });
                },
                icon: Icon(Icons.star))
          ],
        ),
        body: Center(
          child: Text(
              displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
  }

// class HelloFlutterApp extends StatelessWidget{
//   Widget build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello IAMINW"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: (){},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//               "Hello fluter",
//             style: TextStyle(fontSize: 24),
//           ),
//         ),
//       ),
//     );
//   }
//}

